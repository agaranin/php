<!doctype html>
<html lang="en">

<head>
    <title>Register</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <div class="centeredContent">
        <h3>Submit passport</h3>
        <?php
        require_once 'db.php';
        function displayForm()
        {
            $form = <<< END
        <div class="container">
            <form action="" method="POST" enctype="murequire_once 'db.php';ltipart/form-data">
                <div class="row">
                    <div class="col-25">
                        <label for="image">Choose image:</label>
                    </div>
                    <div class="col-75">
                        <input type="file" id="image" name="image">
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="passportNo">Passport No:</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="passportNo" name="passportNo">
                    </div>
                </div>
                <div class="row">
                    <input type="submit" value="Submit">
                </div>
            </form>
        </div>
END;
            echo $form;
        }
        if(isset($_FILES['image'])){
            $errors= array();
            $file_name = $_FILES['image']['name'];
            $file_size =$_FILES['image']['size'];
            $file_tmp =$_FILES['image']['tmp_name'];
            $file_type=$_FILES['image']['type'];
            $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

            $extensions= array("jpeg","jpg","png");

            if(in_array($file_ext,$extensions)=== false){
                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }

            if($file_size > 2097152){
                $errors[]='File size must be exactly 2 MB';
            }

            if(empty($errors)==true){
                move_uploaded_file($file_tmp, "uploads/".$file_name);
                echo "Success";
            }else{
                print_r($errors);
            }
        }
        displayForm();
        ?>
    </div>
</body>

</html>