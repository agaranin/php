<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  if (!isset($_GET['min']) || !isset($_GET['max'])) {
    echo "Error: you must provide min and max values in the URL";
  } elseif ($_GET['min'] > $_GET['max']) {
    echo "Maximum value should be geater than minimum value";
  } elseif (is_int($_GET['min'])==false || is_int($_GET['max'])==false) {
    echo "Values should be integers";
  } else {
    echo "$min";
    $min = $_GET['min'];
    $max = $_GET['max'];
    for ($i = 0; $i < 10; $i++) {
      $rand = rand($min, $max);
      echo "$rand<br>";
    }
  }
  ?>
</body>

</html>