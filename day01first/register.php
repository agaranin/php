<!doctype html>
<html lang="en">
<head>
    <title>Register</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body {
            font-family: "Helvetica", "Arial", "sans-serif";
        }

        .signup-form {
            margin: 0 auto;
            width: 600px;
            padding: 30px;
            border: 1px solid black;
        }

        .row {
            margin-bottom: 30px;
        }

        .btn {
            width: 200px;
        }

        h3 {
            text-align: center;
            margin-top: 100px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
<?php
if (isset($_POST['submit'])){
    $username = $_POST['username'];
    $email = $_POST['email'];
}
?>
<h3>New User Registration</h3>
<div class="signup-form">
    <form  method="post">
        <div class="row">
            <label class="col-6">Desired Username</label>
            <input class="col-6" name="username" type="Text" placeholder="Username" value="<?php echo htmlspecialchars($username) ?>"><br/>
        </div>
        <div class="row">
            <label class="col-6">Your email</label>
            <input class="col-6" name="email" type="email" placeholder="Email" value="<?php echo htmlspecialchars($email) ?>"><br/>
        </div>
        <div class="row">
            <label class="col-6">Password</label>
            <input class="col-6" name="password" type="password" placeholder="Password"><br/>
        </div>
        <div class="row">
            <label class="col-6">Password (repeat)</label>
            <input class="col-6" name="password-repeat" type="password" placeholder="Password"><br/>
        </div>
        <div class="row">
            <div class="col text-center">
                <button class="btn btn-success " name="submit" type="submit">Register!</button>
            </div>
        </div>
    </form>
    <?php
    if (isset($_POST['submit'])){
        $errorList = array();
        if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['password-repeat'])) {
            // array_push($errorList, "Please enter values for submission");
            $errorList[] = "Please enter values for submission";
        } else { // submission received
            $username = $_POST['username'];
            $email = $_POST['email'];
            $pass = $_POST['password'];
            $passRep = $_POST['password-repeat'];
            if (!preg_match('/^[a-z0-9]{4,20}$/', $username)){
                $errorList[] = "Username must be 4-20 characters, consist of lower case letters and numbers";
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errorList[] = "Email is not a valid email address";
            }
            if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,100}$/', $pass)){
                $errorList[] = "Password must be 6-100 characters, one uppercase letter, one lower case letter, and one number.";
            }
            if ($pass != $passRep){
                $errorList[] = "Passwords must match";
            }
        }
        if ($errorList) { // there were errors - display them
            echo '<ul>';
            foreach ($errorList as $error) {
                echo "<li>$error</li>";
            }
            echo '</ul>';
        } else { // submission successful - do the work
            echo "<p>";
            echo "Hello $username, your email is $email, your password is $pass";
            echo "</p>";
        }
    }
    ?>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>