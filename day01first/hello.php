<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  if (!isset($_GET['name'])) {
    echo "Error: you must provide name in the URL";
  } elseif (!isset($_GET['age'])) {
    echo "Error: you must provide age in the URL";
  } else {
    $name = $_GET['name'];
    $age = $_GET['age'];
    echo "Hello $name, nice to meet you!";
    echo "$name is $age y/o";
  }
  ?>
</body>
</html>
