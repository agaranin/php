<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
<div id="centeredContent">
    <?php
    require_once 'db.php';

    function displayForm($name = "", $age = "") {
        $form = <<< END
    <form method="post">
        Name: <input name="name" type="text" value="$name"><br>
        Age: <input name="age" type="number" value="$age"><br>
        <input type="submit" value="Say hello">
    </form>
END;
        echo $form;
    }

    if (isset($_POST['name'])) { // we're receving a submission
        $name = $_POST['name'];
        $age = $_POST['age'];
        // verify inputs
        $errorList = array();
        if (strlen($name) < 2 || strlen($name) > 50) {
            $errorList[] = "Name must be 2-50 characters long";
            $name = "";
        }
        if (filter_var($age, FILTER_VALIDATE_INT) === false || $age < 0 || $age > 150) {
            $errorList[] = "Age must be a number between 0 and 150";
            $age = "";
        }
        //
        if ($errorList) { // STATE 2: submission with errors (failed)
            echo '<ul class="errorMessage">';
            foreach ($errorList as $error) {
                echo "<li>$error</li>\n";
            }
            echo '</ul>';
            displayForm($name, $age);
        } else { // STATE 3: submission successful
            $sql = sprintf("INSERT INTO people VALUES (NULL, '%s', '%s')",
                    mysqli_real_escape_string($link, $name),
                    mysqli_real_escape_string($link, $age)
                );
            if(!mysqli_query($link, $sql)) {
                echo "Fatal error: failed to execute SQL query: " . mysqli_error($link);
            }
            echo "Hello $name, you are $age y/o.";
        }
    } else { // STATE 1: first show
        displayForm();
    }
    ?>

</div>
</body>
</html>