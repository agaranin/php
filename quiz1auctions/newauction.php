<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>New auction</title>
  <link rel="stylesheet" href="styles.css" />
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea[name=description]'
    });
  </script>
</head>

<body>
  <div id="centeredContent">
    <?php
    require_once 'db.php';

    function printForm($description = "", $sName = "")
    {
      $sName = htmlentities($sName); // avoid invalid html in case <>" are part of name
      $form = <<< END
    <form method="post" enctype="multipart/form-data">
        Item description <textarea name="description" cols="60" rows="10">$description</textarea><br>
        Photo (optional): <input type="file" name="photo" /><br>
        Sellers name: <input type="text" name="sname" value="$sName"><br>
        Sellers email: <input type="email" id="email" name="email">
        Initial bid price: <input type="text" name="bidprice" value=""><br>
        <input type="submit" name="submit" value="Add auction">
    </form>
END;
      echo $form;
    }

    // returns TRUE on success
    // returns a string with error message on failure
    function verifyUploadedPhoto(&$photoFilePath) // FIX PHOTO NAME
    {
      if (isset($_FILES['photo']) && $_FILES['photo']['error'] != 4) { // file uploaded
        // print_r($_FILES);
        $photo = $_FILES['photo'];
        $photoName = $photo['name'];
        $photoName = explode('.', $photoName)[0];
        if ($photo['error'] != 0) {
          return "Error uploading photo " . $photo['error'];
        }
        if ($photo['size'] > 1024 * 1024) { // 1MB
          return "File too big. 1MB max is allowed.";
        }
        $info = getimagesize($photo['tmp_name']);
        if (!$info) {
          return "File is not an image";
        }
        // echo "\n\nimage info\n";
        // print_r($info);
        if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
          return "Width and height must be within 200-1000 pixels range";
        }
        $ext = "";
        switch ($info['mime']) {
          case 'image/jpeg':
            $ext = "jpg";
            break;
          case 'image/gif':
            $ext = "gif";
            break;
          case 'image/png':
            $ext = "png";
            break;
          default:
            return "Only JPG, GIF and PNG file types are allowed";
        }
        $photoFilePath = "uploads/" .  $photoName . "." . $ext; // FIX PHOTO NAME
      }
      return TRUE;
    }

    if (isset($_POST['submit'])) { // are we receiving a submission?
      $description = $_POST['description'];
      $description = strip_tags($description, "<p><ul><li><em><strong><i><bold><ol><span><hr><br>");
      $sName = $_POST['sname'];
      $email = $_POST['email'];
      $bidPrice = $_POST['bidprice'];
      $errorList = array();
      if  (strlen($description) < 2 || strlen($description) > 1000) {
        $errorList[] = "Description  must be 2-1000";
      }
      if (strlen($sName) < 2 || strlen($sName) > 100) {
        $errorList[] = "Name  must be 2-100";
      }
      if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList[] = "Email does not look valid";
        $email = "";
      }
      // TODO: verify the picture upload is acceptable
      $photoFilePath = null;  // in SQL INSERT query this must become NULL and *not* 'NULL'
      $retval = verifyUploadedPhoto($photoFilePath); //FIX NAME
      if ($retval !== TRUE) {
        $errorList[] = $retval; // string with error was returned - add it to list of errors
      }
      // it's okay if no photo was selected - we will just insert NULL value
      //
      if ($errorList) { // STATE 2: errors in submission - failed
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
          echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        printForm($description, $sName);
      } else { // STATE 3: successful submission
        if ($photoFilePath != null) {
          if (move_uploaded_file($_FILES['photo']['tmp_name'], $photoFilePath) != true) {
            die("Error moving the uploaded file. Action aborted.");
          }
        }
        $sql = sprintf(
          "INSERT INTO auctions VALUES (NULL, '%s', %s, '%s', '%s', '%s', NULL, NULL)",
          mysqli_real_escape_string($link, $description),
          ($photoFilePath == null) ? "NULL" : "'" . mysqli_real_escape_string($link, $photoFilePath) . "'",
          mysqli_real_escape_string($link, $sName),
          mysqli_real_escape_string($link, $email),
          mysqli_real_escape_string($link, $bidPrice),
        );
        $result = mysqli_query($link, $sql);
        if (!$result) {
          die("SQL Query failed: " . mysqli_error($link));
        }

        echo "<p>Auction successfully added</p>";
      }
    } else { // STATE 1: first display
      printForm();
    }

    ?>
  </div>
</body>

</html>