<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="styles.css" />
  <title>List Items</title>
</head>

<body>
  <div id="centeredContent">
    <table style="width:100%">
      <tr>
        <th>Item description</th>
        <th>Image</th>
        <th>Sellers name</th>
        <th>last bid price</th>
        <th>make a bid</th>
      </tr>
      <?php
      require_once 'db.php';

      $result = mysqli_query($link, sprintf("
      SELECT *
      FROM auctions
      "));
      if (!$result) {
        echo "SQL Query failed: " . mysqli_error($link);
        exit;
      }
      $userRecord = mysqli_fetch_all($result);
      //print_r($userRecord[0]);
      for ($i = 0; $i < count($userRecord); $i++) {
        $description = $userRecord[$i][1];
        $photo = $userRecord[$i][2];
        $sName = $userRecord[$i][3];
        $lastBidPrice = $userRecord[$i][5];
        $makeBid = "<a href=\"placebid.php?id={$userRecord[$i][0]}\">make a bid</a>";
        echo '<tr>';
        echo "<td>$description</td>";
        echo "<td><img src=\"$photo\" width=\"150\"></td>";
        echo "<td>$sName</td>";
        echo "<td>$lastBidPrice</td>";
        echo "<td>$makeBid</td>";
        echo '</tr>';
      }
      ?>
    </table>
  </div>
</body>

</html>