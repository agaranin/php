<?php
require_once 'vendor/autoload.php';

DB::$dbName = 'day02people';
DB::$user = 'day02people';
DB::$host ='localhost';
DB::$password ='YJyBP8f0iOYBAHP8';
//DB::$port = 3333;

// Create and configure Slim app
$config = ['settings' => [
  'addContentLengthHeader' => false,
  'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/cache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

// Define app routes
$app->get('/hello/{name}', function ($request, $response, $args) {
  return $response->write("Hello " . $args['name']);
});

$app->get('/hello/{nameX}/{ageX}', function ($request, $response, $args) {
  //return $response->write("<p>Hello " . $args['name'] . " you are " . $args['age'] . " Y/O</p>");
$name = $args['nameX'];
$age = $args['ageX'];
DB::insert('people', ['name' => $name, 'age' => $age]);
  return $this->view->render($response, 'hello.html.twig', ['nameZZZ' => $name, 'ageZZZ' => $age]);
});

//STATE 1: first display of the form
$app->get('/addperson', function ($request, $response, $args) {
  return $this->view->render($response, 'addperson.html.twig');
});

//STATE 2&3: receiving submission
$app->post('/addperson', function ($request, $response, $args) {
  $name = $request->getParam('name');
  $age = $request->getParam('age');
  $errorList = [];
  if (strlen($name) < 2 || strlen($name) > 50) {
    $errorList[] = "Name must be 2-50 characters long";
    $name = "";
  }
  if (filter_var($age, FILTER_VALIDATE_INT) === false || $age < 0 || $age > 150) {
    $errorList[] = "Age must be a number between 0 and 150";
    $age = "";
  }
  //
  if($errorList){ //STATE 2: errors - redisplay the form
    $valuesList = ['name' => $name, 'age' => $age];
      return $this->view->render($response, 'addperson.html.twig', ['errorList' => $errorList, 'v' => $valuesList]);
  } else{ // STATE 3: SUCCESS
    DB::insert('people',['name' => $name, 'age' => $age]);
    return $this->view->render($response, 'addperson_success.html.twig');
  }
  
});

// Run app
$app->run();