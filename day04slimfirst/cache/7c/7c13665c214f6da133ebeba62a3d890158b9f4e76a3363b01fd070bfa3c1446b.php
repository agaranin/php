<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hello.html.twig */
class __TwigTemplate_36e4717b08d133367ec62cc9254ee362971c5bdb138e1f96bc65a789c2b29db9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p>Hello (from twig) ";
        echo twig_escape_filter($this->env, ($context["nameZZZ"] ?? null), "html", null, true);
        echo " you are ";
        echo twig_escape_filter($this->env, ($context["ageZZZ"] ?? null), "html", null, true);
        echo " y.o.</p>";
    }

    public function getTemplateName()
    {
        return "hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p>Hello (from twig) {{ nameZZZ }} you are {{ ageZZZ }} y.o.</p>", "hello.html.twig", "/opt/lampp/htdocs/ipd23/DAY04SLIMFIRST/templates/hello.html.twig");
    }
}
