<?php
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Register</title>
</head>
<body>
<div class="centeredContent">
    <div align="center">
        <marquee behavior="alternate" bgcolor="#bb3434" direction="left" height:=""
                 loop="7" scrollamount="1" scrolldelay="2" width="100%">
 <span class="banner">
 Latest news! Latest news! Latest news! Latest news!</span></marquee>
    </div>
    <div class="topnav">
        <a href="index.php">Home</a>
        <a href="article.php">Articles</a>
        <a href="articleadd.php">Add</a>
        <a href="login.php">Login</a>
        <a href="register.php">Register</a>
    </div>
    <h2>User login</h2>
    <?php
    function displayForm($username = "", $email = "") {
        $form = <<< END
<div class="container">
  <form method="post">
  <div class="row">
    <div class="col-25">
      <label for="username">Username:</label>
    </div>
    <div class="col-75">
      <input type="text" id="username" name="username" value="$username">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="password">Password:</label>
    </div>
    <div class="col-75">
        <input type="password" id="password" name="password">
    </div>
  </div>
  <div class="row">
    <input type="submit" value="Login">
  </div>
  <div class="row">
    <a href="register.php">No account? Register here.</a>
  </div>
  </form>
</div>
END;
        echo $form;
    }

    if (isset($_POST['username'])) { // we're receving a submission
        $username = $_POST['username'];
        $password = $_POST['password'];
        // verify inputs
        $result = mysqli_query($link, sprintf("SELECT * FROM users WHERE username='%s'",
            mysqli_real_escape_string($link, $username)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        $userRecord = mysqli_fetch_assoc($result);
        $loginSuccessful = false;
        if ($userRecord) {
            if ($userRecord['password'] == $password) {
                $loginSuccessful = true;
            }
        }
        //
        if (!$loginSuccessful) { // STATE 2: submission with errors (failed)
            echo '<p class="errorMessage">Invalid username or password</p>';
            displayForm();
        } else { // STATE 3: submission successful
            unset($userRecord['password']); // for safety reasons remove the password
            $_SESSION['blogUser'] = $userRecord;
            echo "<p>login successful</p>";
            echo '<p><a href="index.php">Click here to continue</a></p>';
        }
    } else { // STATE 1: first show
        displayForm();
    }
    ?>

    <div class="footer">
        <p>All Rights Reserved.</p>
    </div>
</div>
</body>
</html>