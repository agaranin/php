<?php
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Create new article</title>
</head>
<body>
<div class="centeredContent">
    <div align="center">
        <marquee behavior="alternate" bgcolor="#bb3434" direction="left" height:=""
                 loop="7" scrollamount="1" scrolldelay="2" width="100%">
 <span class="banner">
 Latest news! Latest news! Latest news! Latest news!</span></marquee>
    </div>
    <div class="topnav">
        <a href="index.php">Home</a>
        <a href="article.php">Articles</a>
        <a href="articleadd.php">Add</a>
        <a href="login.php">Login</a>
        <a href="register.php">Register</a>
    </div>
    <?php
    if (isset($_SESSION['blogUser'])) {
        $username = $_SESSION['blogUser']['username'];
        echo '<p id="login">';
        echo "You are logged in as $username. ";
        echo '<a href="logout.php"> Logout</a></br>';
        echo '<a href="articleadd.php"> submit a new article</a>';
        echo '</p></br>';

        $result = mysqli_query($link, sprintf("
SELECT a.title as title, u.username as username, a.createdTS as createdTS, a.body as body, a.id as articleId 
FROM articles as a 
INNER JOIN users as u
ON a.authorId = u.id
ORDER BY a.id DESC LIMIT 5
"));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        $userRecord = mysqli_fetch_all($result);
        print_r($userRecord[1]);
        echo '<p id="login">';

        for ($i=0; $i < 5; $i++) {
            $articleId = $userRecord[$i][4];
            $postedDate = date('M d, Y \a\t H:i:s', strtotime($userRecord[$i][2]));
            echo "</br><a href=\"article.php?id=$articleId\"> {$userRecord[$i][0]}</a>";
            echo "</br>Posted by {$userRecord[$i][1]} on $postedDate";
            echo "</br>{$userRecord[$i][3]}";
        }
        echo '</p></br>';

    } else {
        echo '<p id="login">';
        echo '<a href="login.php">Login</a> or ';
        echo '<a href="register.php.php">Register </a>';
        echo 'to post articles and comments';
        echo '</p></br>';
    }
    ?>
    <div class="footer">
        <p>All Rights Reserved.</p>
    </div>
</div>
</body>
</html>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cum eveniet id, nam neque pariatur perferendis praesentium quo repellendus reprehenderit soluta tenetur, veniam? Consectetur ea error explicabo, impedit incidunt maiores molestiae omnis pariatur quo tempore! Ab alias atque dignissimos facilis laboriosam, maxime odit officia ratione reiciendis sed, tempora unde, veniam!