<?php
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Create new article</title>
</head>
<body>
<div class="centeredContent">
    <div align="center">
        <marquee behavior="alternate" bgcolor="#bb3434" direction="left" height:=""
                 loop="7" scrollamount="1" scrolldelay="2" width="100%">
 <span class="banner">
 Latest news! Latest news! Latest news! Latest news!</span></marquee>
    </div>
    <div class="topnav">
        <a href="index.php">Home</a>
        <a href="article.php">Articles</a>
        <a href="articleadd.php">Add</a>
        <a href="login.php">Login</a>
        <a href="register.php">Register</a>
    </div>
    <?php

    ?>
    <?php
    function displayForm($title = "", $content = "")
    {
        $form = <<< END
<h2>Create new article</h2>
<div class="container">
  <form method="post">
  <div class="row">
    <div class="col-25">
      <label for="title">Title</label>
    </div>
    <div class="col-75">
      <input type="text" id="title" name="title">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="content">Content</label>
    </div>
    <div class="col-75">
      <textarea id="subject" name="content" style="height:200px"></textarea>
    </div>
  </div>
  <div class="row">
    <input type="submit" value="Create">
  </div>
  </form>
</div>
END;
        echo $form;
    }
    if (isset($_SESSION['blogUser'])) {
        $username = $_SESSION['blogUser']['username'];
        echo '<p id="login">';
        echo "You are logged in as $username. ";
        echo '<a href="logout.php"> Logout</a></br>';
        echo '<a href="articleadd.php"> submit a new article</a>';
        echo '</p></br>';

    if (isset($_POST['title'])) { // we're receiving a submission
        $title = $_POST['title'];
        $content = $_POST['content'];
        // verify inputs
        $errorList = array();
        if (strlen($title) < 10) {
            $errorList[] = "Article title must be at least 10 characters long.";
        }
        if (strlen($content) < 50) {
            $errorList[] = "Article content must be at least 50 characters long.";
        }

        if ($errorList) { // STATE 2: submission with errors (failed)
            echo '<ul class="errorMessage">';
            foreach ($errorList as $error) {
                echo "<li>$error</li>\n";
            }
            echo '</ul>';
            displayForm($title, $content);
        } else { // STATE 3: submission successful
            $usernameId = $_SESSION['blogUser']['id'];
            $sql = sprintf("INSERT INTO articles VALUES (NULL, '%s', NULL, '%s', '%s')",
            mysqli_real_escape_string($link, $usernameId),
                mysqli_real_escape_string($link, $title),
                mysqli_real_escape_string($link, $content)
            );
            if (!mysqli_query($link, $sql)) {
                echo "Fatal error: failed to execute SQL query: " . mysqli_error($link);
            }
            echo '<p style="clear:both">Article added successfully</p>';
            $articleId = mysqli_insert_id($link);
            echo '<p><a href="article.php?id='.$articleId.'">Click here to view it</a></p>';
        }
    } else { // STATE 1: first show
        displayForm();
    }
    } else {
        echo '<p id="login">';
        echo "Not logged in";
        echo '</p></br>';
        echo "<p> Log in to access this page.</p>";
    }
    ?>
    <div class="footer">
        <p>All Rights Reserved.</p>
    </div>
</div>
</body>
</html>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, eaque eum impedit in mollitia natus nisi nobis non obcaecati possimus praesentium quam. Eos eveniet quasi sequi ut voluptas! A accusamus aliquid atque cupiditate dolore doloribus eaque fugit, id illo laborum magnam, nesciunt obcaecati omnis porro possimus repudiandae tempore ullam voluptate!