<?php
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('input[name=username]').keyup(function () {
                var username = $(this).val();
                //console.log('username:' + username);
                username = encodeURIComponent(username);
                $('#usernameTaken').load("isusernametaken.php?username=" + username);
            });
        });
    </script>
    <title>Register</title>
</head>
<body>
<div class="centeredContent">
    <div align="center">
        <marquee behavior="alternate" bgcolor="#bb3434" direction="left" height:=""
                 loop="7" scrollamount="1" scrolldelay="2" width="100%">
 <span class="banner">
 Latest news! Latest news! Latest news! Latest news!</span></marquee>
    </div>
    <div class="topnav">
        <a href="index.php">Home</a>
        <a href="article.php">Articles</a>
        <a href="articleadd.php">Add</a>
        <a href="login.php">Login</a>
        <a href="register.php">Register</a>
    </div>

    <!--    <div class="centeredVertically">-->
    <h2>New User registration</h2>
    <?php
    function displayForm($username = "", $email = "")
    {
        $form = <<< END
<div class="container">
  <form method="post">
  <div class="row">
    <div class="col-25">
      <label for="username">Username:</label>
    </div>
    <div class="col-75">
      <input type="text" id="username" name="username">
      <span id="usernameTaken" class="errorMessage"></span></br>
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="email">Email:</label>
    </div>
    <div class="col-75">
      <input type="email" id="email" name="email">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="pass1">Password:</label>
    </div>
    <div class="col-75">
        <input type="password" id="pass1" name="pass1">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="password">Password (repeated):</label>
    </div>
    <div class="col-75">
        <input type="password" id="pass2" name="pass2">
    </div>
  </div>
  <div class="row">
    <input type="submit" value="Register">
  </div>
  </form>
</div>
END;
        echo $form;
    }

    if (isset($_POST['username'])) { // we're receiving a submission
        $username = $_POST['username'];
        $email = $_POST['email'];
        $pass1 = $_POST['pass1'];
        $pass2 = $_POST['pass2'];
        // verify inputs
        $errorList = array();
        if (preg_match('/^[a-z0-9]{4,20}$/', $username) != 1) {
            $errorList[] = "Username must be 4-20 characters long made up of lower-case characters and numbers";
            $username = "";
        } else { // but is this username already in use?
            // FIXME !!! Homework
            $result = mysqli_query($link, sprintf("SELECT * FROM users WHERE username='%s'",
                mysqli_real_escape_string($link, $username)));
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            $userRecord = mysqli_fetch_assoc($result);
            if ($userRecord) {
                $errorList[] = "This username is already registered";
                $username = "";
            }
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
            $errorList[] = "Email does not look valid";
            $email = "";
        } else { // but is this email already in use?
            $result = mysqli_query($link, sprintf("SELECT * FROM users WHERE email='%s'",
                mysqli_real_escape_string($link, $email)));
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            $userRecord = mysqli_fetch_assoc($result);
            if ($userRecord) {
                $errorList[] = "This email is already registered";
                $email = "";
            }
        }
        if ($pass1 != $pass2) {
            $errorList[] = "Passwords do not match";
        } else {
            // maybe? preg_match("/^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{6,100}$/"
            if (strlen($pass1) < 6 || strlen($pass1) > 100
                || (preg_match("/[A-Z]/", $pass1) == FALSE)
                || (preg_match("/[a-z]/", $pass1) == FALSE)
                || (preg_match("/[0-9]/", $pass1) == FALSE)) {
                $errorList[] = "Password must be 6-100 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it";
            }
        }
        //
        if ($errorList) { // STATE 2: submission with errors (failed)
            echo '<ul class="errorMessage">';
            foreach ($errorList as $error) {
                echo "<li>$error</li>\n";
            }
            echo '</ul>';
            displayForm($username, $email);
        } else { // STATE 3: submission successful
            $sql = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                mysqli_real_escape_string($link, $username),
                mysqli_real_escape_string($link, $email),
                mysqli_real_escape_string($link, $pass1)
            );
            if (!mysqli_query($link, $sql)) {
                echo "Fatal error: failed to execute SQL query: " . mysqli_error($link);
            }
            echo "<p>Registration successful</p>";
            echo '<p><a href="login.php">Click here to login</a></p>';
        }
    } else { // STATE 1: first show
        displayForm();
    }
    ?>
    <!--    </div>-->

    <div class="footer">
        <p>All Rights Reserved.</p>
    </div>
</div>
</body>
</html>