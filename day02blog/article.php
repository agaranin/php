<?php
require_once 'db.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    function showComments($id, $link)
    {
        $sql = "SELECT u.username as username, c.createdTS as createdTS, c.body as comment
                    FROM comments as c, users as u 
                    WHERE c.authorId = u.id
                    AND c.articleId = $id";
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL Query failed: " . mysqli_error($link));
        }
        echo '<p><strong>Previous comments:</strong></p>';
        while ($article = mysqli_fetch_assoc($result)) {
            echo '<div class="commentBox"><p>';
            $postedDate = date('M d, Y \a\t H:i:s', strtotime($article['createdTS']));
            echo "{$article['username']} said on $postedDate</br>";
            echo "{$article['comment']}";
            echo '</p></div>';
        }
    }
}
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Register</title>
</head>
<body>
<div class="centeredContent">
    <div align="center">
        <marquee behavior="alternate" bgcolor="#bb3434" direction="left" height:=""
                 loop="7" scrollamount="1" scrolldelay="2" width="100%">
 <span class="banner">
 Latest news! Latest news! Latest news! Latest news!</span></marquee>
    </div>
    <div class="topnav">
        <a href="index.php">Home</a>
        <a href="article.php">Articles</a>
        <a href="articleadd.php">Add</a>
        <a href="login.php">Login</a>
        <a href="register.php">Register</a>
    </div>
    <?php
    if (!isset($_GET['id'])) {
        die("Error: missing article ID in the URL");
    }
    if (isset($_SESSION['blogUser'])) {
        $username = $_SESSION['blogUser']['username'];
        echo '<p id="login">';
        echo "You are logged in as $username. ";
        echo '<a href="logout.php"> Logout</a></br>';
        echo '<a href="articleadd.php"> submit a new article</a>';
        echo '</p></br>';

        //$id = $_GET['id'];
        $sql = sprintf("
SELECT a.title as title, u.username as username, a.createdTS as createdTS, a.body as body
FROM articles as a
INNER JOIN users as u
ON a.authorId = u.id
WHERE a.id='%s'
", mysqli_real_escape_string($link, $id));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL Query failed: " . mysqli_error($link));
        }
        $article = mysqli_fetch_assoc($result);
        if ($article) {
            echo '<div class="articleBox">';
            echo '<h2>' . htmlentities($article['title']) . '</h2>';
            $postedDate = date('M d, Y \a\t H:i:s', strtotime($article['createdTS']));
            echo '<i> Posted by ' . $article['username'] . ' on ' . $postedDate . "</i>\n";
            echo '<div class="articleBody">' . $article['body'] . "</div>\n";
            echo '</div>';

            function displayCommentForm($comment = "")
            {
                $form = <<< END
<div>
  <form method="post">
  <div class="row">
    <div class="col-25">
      <label for="submit">My comment:</label>
      <input type="submit" value="Add comment">
    </div>
    <div class="col-75">
      <textarea id="comment" name="comment" style="height:200px"></textarea>
    </div>
  </div>
  </form>
</div>
END;
                echo $form;
            }

            if (isset($_POST['comment'])) { // we're receiving a submission
                $comment = $_POST['comment'];
                // verify inputs
                $errorList = array();
                if (strlen($comment) < 1) {
                    $errorList[] = "Enter your comment";
                }
                if ($errorList) { // STATE 2: submission with errors (failed)
                    echo '<ul class="errorMessage">';
                    foreach ($errorList as $error) {
                        echo "<li>$error</li>\n";
                    }
                    echo '</ul>';
                    displayCommentForm();
                    showComments($id, $link);
                } else { // STATE 3: submission successful
                    displayCommentForm();
                    $usernameId = $_SESSION['blogUser']['id'];
                    $comment = $_POST['comment'];
                    $sql = sprintf("INSERT INTO comments VALUES (NULL, '%s', '%s', NULL, '%s')",
                        mysqli_real_escape_string($link, $id),
                        mysqli_real_escape_string($link, $usernameId),
                        mysqli_real_escape_string($link, $comment)
                    );
                    if (!mysqli_query($link, $sql)) {
                        echo "Fatal error: failed to execute SQL query: " . mysqli_error($link);
                    }
                    echo '<p>Comment added successfully</p>';
                    $commentId = mysqli_insert_id($link);
                    showComments($id, $link);

                }
            } else { // STATE 1: first show
                displayCommentForm();
                showComments($id, $link);
            }

        } else {
            echo '<h2>Article not found</h2>';
        }

    } else {
        echo '<p id="login">';
        echo '<a href="login.php">Login</a> or ';
        echo '<a href="register.php">Register </a>';
        echo 'to post articles and comments';
        echo '</p></br>';
    }
    ?>
    <div class="footer">
        <p>All Rights Reserved.</p>
    </div>
</div>
</body>
</html>