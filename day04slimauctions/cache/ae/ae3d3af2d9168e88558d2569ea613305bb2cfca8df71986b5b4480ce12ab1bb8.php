<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_6e40d3525d346257de1757dec8a205273ac5100623214827871c20dde22be18c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "\t</head>
\t<body>
\t\t<div id=\"centeredContent\"> ";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        // line 13
        echo "\t\t\t</div>
\t\t\t<div id=\"footer\">
\t\t\t\t";
        // line 15
        $this->displayBlock('footer', $context, $blocks);
        // line 19
        echo "\t\t\t</div>
\t\t</body>
\t</html>
";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "\t\t\t<link rel=\"stylesheet\" href=\"styles.css\"/>
\t\t\t<title>
\t\t\t\t";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        // line 8
        echo "\t\t\t\tDay04SlimAuctions</title>
\t\t";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 12
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 15
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "\t\t\t\t\t&copy; Copyright 2011 by
\t\t\t\t\t<a href=\"http://domain.invalid/\">you</a>.
\t\t\t\t";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  98 => 16,  94 => 15,  88 => 12,  82 => 7,  77 => 8,  75 => 7,  71 => 5,  67 => 4,  60 => 19,  58 => 15,  54 => 13,  52 => 12,  48 => 10,  46 => 4,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
\t<head>
\t\t{% block head %}
\t\t\t<link rel=\"stylesheet\" href=\"styles.css\"/>
\t\t\t<title>
\t\t\t\t{% block title %}{% endblock %}
\t\t\t\tDay04SlimAuctions</title>
\t\t{% endblock %}
\t</head>
\t<body>
\t\t<div id=\"centeredContent\"> {% block content %}{% endblock %}
\t\t\t</div>
\t\t\t<div id=\"footer\">
\t\t\t\t{% block footer %}
\t\t\t\t\t&copy; Copyright 2011 by
\t\t\t\t\t<a href=\"http://domain.invalid/\">you</a>.
\t\t\t\t{% endblock %}
\t\t\t</div>
\t\t</body>
\t</html>
", "master.html.twig", "/Applications/AMPPS/www/ipd23/day04slimauctions/templates/master.html.twig");
    }
}
