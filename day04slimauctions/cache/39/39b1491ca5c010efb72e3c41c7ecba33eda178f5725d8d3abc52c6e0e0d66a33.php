<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* listitems.html.twig */
class __TwigTemplate_34cbbfb93be34c13db33eccaa3f1241836016ed51ac26b14df25db30f3d9d62c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "listitems.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t<div id=\"centeredContent\">
\t\t<table style=\"width:100%\">
\t\t\t<tr>
\t\t\t\t<th>Item description</th>
\t\t\t\t<th>Image</th>
\t\t\t\t<th>Sellers name</th>
\t\t\t\t<th>last bid price</th>
\t\t\t\t<th>make a bid</th>
\t\t\t</tr>
\t\t\t";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["results"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 14
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "itemDescription", [], "any", false, false, false, 15), "html", null, true);
            echo "</td>
\t\t\t\t\t<td><img src=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "itemImagePath", [], "any", false, false, false, 16), "html", null, true);
            echo "\" width=\"150\"></td>
\t\t\t\t\t<td>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "sellersName", [], "any", false, false, false, 17), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "lastBidPrice", [], "any", false, false, false, 18), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<a href=\"placebid.php?id=";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "id", [], "any", false, false, false, 20), "html", null, true);
            echo "\">make a bid</a>
\t\t\t\t\t</td>

\t\t\t\t</tr>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "
\t\t</table>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "listitems.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 25,  85 => 20,  80 => 18,  76 => 17,  72 => 16,  68 => 15,  65 => 14,  61 => 13,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
\t<div id=\"centeredContent\">
\t\t<table style=\"width:100%\">
\t\t\t<tr>
\t\t\t\t<th>Item description</th>
\t\t\t\t<th>Image</th>
\t\t\t\t<th>Sellers name</th>
\t\t\t\t<th>last bid price</th>
\t\t\t\t<th>make a bid</th>
\t\t\t</tr>
\t\t\t{% for row in results %}
\t\t\t\t<tr>
\t\t\t\t\t<td>{{row.itemDescription}}</td>
\t\t\t\t\t<td><img src=\"{{ row.itemImagePath }}\" width=\"150\"></td>
\t\t\t\t\t<td>{{row.sellersName}}</td>
\t\t\t\t\t<td>{{row.lastBidPrice}}</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<a href=\"placebid.php?id={{row.id}}\">make a bid</a>
\t\t\t\t\t</td>

\t\t\t\t</tr>
\t\t\t{% endfor %}

\t\t</table>
\t</div>

{% endblock content %}
", "listitems.html.twig", "/opt/lampp/htdocs/ipd23/day04slimauctions/templates/listitems.html.twig");
    }
}
