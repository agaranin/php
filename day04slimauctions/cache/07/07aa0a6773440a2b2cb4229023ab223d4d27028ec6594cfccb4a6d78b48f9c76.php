<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* newauction.html.twig */
class __TwigTemplate_f4e3cbf6bf11bc023c5e148bd2d720777451ddf9c9a02de6d58685a8b88606a6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "newauction.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
";
        // line 5
        if (($context["errorList"] ?? null)) {
            // line 6
            echo "\t<ul>
\t\t";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 8
                echo "\t\t\t<li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "\t</ul>
";
        }
        // line 12
        echo "
<form method=\"post\" enctype=\"multipart/form-data\">
\tItem description
<textarea name=\"description\" cols=\"60\" rows=\"10\">";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "description", [], "any", false, false, false, 15), "html", null, true);
        echo "</textarea><br>
\tSellers name:
<input type=\"text\" name=\"sname\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "sName", [], "any", false, false, false, 17), "html", null, true);
        echo "\"><br>
\tSellers email:
<input type=\"email\" id=\"email\" name=\"email\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 19), "html", null, true);
        echo "\"><br>
\tInitial bid price:
<input type=\"number\" name=\"bidprice\" value=\"\" step=\".01\"><br>

Photo (optional):
<input type=\"file\" name=\"photo\"/><br>
\t<input type=\"submit\" name=\"submit\" value=\"Add auction\">
</form>

";
    }

    public function getTemplateName()
    {
        return "newauction.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 19,  85 => 17,  80 => 15,  75 => 12,  71 => 10,  62 => 8,  58 => 7,  55 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}

{% if errorList %}
\t<ul>
\t\t{% for error in errorList %}
\t\t\t<li>{{ error }}</li>
\t\t{% endfor %}
\t</ul>
{% endif %}

<form method=\"post\" enctype=\"multipart/form-data\">
\tItem description
<textarea name=\"description\" cols=\"60\" rows=\"10\">{{ v.description }}</textarea><br>
\tSellers name:
<input type=\"text\" name=\"sname\" value=\"{{ v.sName }}\"><br>
\tSellers email:
<input type=\"email\" id=\"email\" name=\"email\" value=\"{{ v.email }}\"><br>
\tInitial bid price:
<input type=\"number\" name=\"bidprice\" value=\"\" step=\".01\"><br>

Photo (optional):
<input type=\"file\" name=\"photo\"/><br>
\t<input type=\"submit\" name=\"submit\" value=\"Add auction\">
</form>

{% endblock content %}", "newauction.html.twig", "/opt/lampp/htdocs/ipd23/day04slimauctions/templates/newauction.html.twig");
    }
}
