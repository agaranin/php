<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$dbName = 'day12travelapi';
DB::$user = 'day12travelapi';
DB::$password = 'izMSsTnH0tcXoUSX';
DB::$host = 'localhost';
DB::$port = 3333;

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    global $log, $container;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // this was tricky to find - getting access to twig rendering directly, without PHP Slim
    http_response_code(500); // internal server error
    header('Content-type: application/json; charset=UTF-8');
    die(json_encode("500 - Internal error"));
}

// Create and configure Slim app

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);
$container = $app->getContainer();

//Override the default Not Found Handler before creating App
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    };
};

// API calls handlers are below

$app->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Todo app with RESTful API");
    return $response;
});

$app->get('/people', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $list = DB::query("SELECT * FROM people");
    $json = json_encode($list, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

// fetch one record
$app->get('/people/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $item = DB::queryFirstRow("SELECT * FROM people WHERE id=%i", $args['id']);
    if (!$item) {
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($item, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->post('/people', function (Request $request, Response $response, array $args) use ($log) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $item = json_decode($json, TRUE); // true makes it return an associative array instead of an object
    // validate

    if (($result = validatePeople($item)) !== TRUE) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    DB::insert('people', $item);
    $insertId = DB::insertId();
    $log->debug("Record people added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    return $response;
});

$app->map(['PUT', 'PATCH'], '/people/{id:[0-9]+}', function (Request $request, Response $response, array $args) use ($log) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $item = json_decode($json, true); // true makes it return an associative array instead of an object
    // TODO: validate
    $method = $request->getMethod();
    $origItem = DB::queryFirstRow("SELECT * FROM people WHERE id=%i", $args['id']);
    if ($origItem) {
        if (($result = validatePeople($item, $method == 'PATCH')) !== true) {
            if ($result !== "Passport already exist") {
                $response = $response->withStatus(400);
                $response->getBody()->write(json_encode("400 - " . $result));
                return $response;
            }
        }
    }
    if (!$origItem) { // record not found
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    DB::update('people', $item, "id=%i", $args['id']);
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
    return $response;
});

// delete one record
$app->delete('/people/{id:[0-9]+}',  function (Request $request, Response $response, array $args) use ($log) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    DB::delete('people', "id=%i", $args['id']);
    $log->debug("Record people deleted id=" . $args['id']);
    // code is always 200
    // return true if record actually deleted, false if it did not exist in the first place
    $count = DB::affectedRows();
    $json = json_encode($count != 0, JSON_PRETTY_PRINT); // true or false
    return $response->getBody()->write($json);
});

// returns TRUE if all is fine otherwise returns string describing the problem
function validatePeople($people, $forPatch = false)
{
    if ($people === NULL) { // probably json_decode failed due to JSON syntax errors
        return "Invalid JSON data provided";
    }
    // - only allow the fields that must/can be present
    $expectedFields = ['name', 'age', 'passport'];
    $peopleFields = array_keys($people); // get names of fields as an array
    // check if there are any fields that should not be there
    if ($diff = array_diff($peopleFields, $expectedFields)) {
        return "Invalid fields in People: [" . implode(',', $diff) . "]";
    }
    //
    if (!$forPatch) { // is it PUT or POST
        // - check if any fields are missing that must be there
        if ($diff = array_diff($expectedFields, $peopleFields)) {
            return "Missing fields in People: [" . implode(',', $diff) . "]";
        }
    }
    // do not allow any fields to be null - database would not accept it
    $nullableFields = ['passport']; // put list of nullable fields here
    foreach ($people as $key => $value) {
        if (!in_array($key, $nullableFields)) {
            if (@is_null($value)) { // @ is to suppress a warning (which would be printed out)
                return "$key must not be null";
            }
        }
    }
    // - name 1-100 characters long
    if (isset($people['name'])) {
        $name = $people['name'];
        if (preg_match('/^[a-zA-Z0-9 ,\.-:\'_]{2,50}$/', $name) !== 1) {
            return "People name must be 2-50 characters, only letters (uppercase or lowercase), numbers .,:'_- and space";
        }
    }

    // - Age a valid Int 1-150 range
    if (isset($people['age'])) {
        $age = $people['age'];
        if (is_numeric($age)) {
            // $people = (int)$people;
            if (filter_var($age, FILTER_VALIDATE_INT) === false) {
                return "Age must be an integer";
            }
            if ($age < 1 || $age > 150) {
                return "Age must be between 1 and 150";
            }
        } else {
            return "Age must a number";
        }
    }

    // - passport UNIQUE NULL ALLOWED !! // AB123456 format or NULL
    if (isset($people['passport'])) {
        $passport = $people['passport'];
        if (!is_null($passport)) {
            if (preg_match('/^[A-Z]{2}[0-9]{6}$/', $passport) !== 1) {
                return "Passport should match AB123456 format or NULL";
            }
        }

        if (!is_null($passport)) {
            $passportDb = DB::queryFirstRow("SELECT passport FROM people WHERE passport=%s", $passport);
            if ($passportDb) {
                return "Passport already exist";
            }
        }
    }
    // if we passed all tests return TRUE
    return TRUE;
}


// Run app - must be the last operation
// if you forget it all you'll see is a blank page
$app->run();
